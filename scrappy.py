from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException

YEARS = range(2012, 2018)
MONTHS = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep',
          'Oct', 'Nov', 'Dec']
MONTHS_DURATION = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
URL = ('https://www.wunderground.com/history/monthly/VILK/date/{}-{}?req_city'
       '=Lucknow&req_state=UP&req_statename=India&reqdb.zip=00000&reqdb.magic'
       '=21&reqdb.wmo=42368')
TABLE_XPATH = ('/html/body/app/city-history/city-history-layout/div/div[2]/'
               'section/div[2]/div[3]/div/div[1]/div/div/'
               'city-history-observation/div/div[2]/table')

TIMEOUT = 15


def is_leap_year(year):
    return not (year % 4)


def get_rough_data(browser, year, month):
    browser.get(URL.format(year, month))
    try:
        WebDriverWait(browser, TIMEOUT).until(
                EC.visibility_of_element_located((By.XPATH, TABLE_XPATH)))
    except TimeoutException:
        print("Timed out waiting for page to load")
        browser.quit()

    elem = browser.find_elements_by_xpath(TABLE_XPATH)
    table = [e.text for e in elem]
    rough_data = table[0].split("\n")
    del(rough_data[0])
    return rough_data


def main():
    browser = webdriver.Firefox()
    for year in YEARS:
        for k in range(len(MONTHS)):
            rough_data = get_rough_data(browser, year, k+1)
            print(rough_data)
            with open('sample.csv', 'a') as f:
                month_duration = 30 if ((k == 1) and is_leap_year(
                        year)) else MONTHS_DURATION[k] + 1
                print(month_duration)
                for i in range(1, month_duration):
                    f.write("{} {} {}".format(MONTHS[k], rough_data[i], year))
                    f.write(',')
                    f.write(rough_data[i+month_duration])
                    f.write(',')
                    f.write(rough_data[i+month_duration*2])
                    f.write(',')
                    f.write(rough_data[i+month_duration*3])
                    f.write(',')
                    f.write(rough_data[i+month_duration*4])
                    f.write(',')
                    f.write(rough_data[i+month_duration*5])
                    f.write(',')
                    f.write(rough_data[i+month_duration*6])
                    f.write('\n')


if __name__ == '__main__':
    main()
